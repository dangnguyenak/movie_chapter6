<?php
namespace Magenest\Movie\Controller\Adminhtml\Movielist;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\App\Action;
use Magenest\Movie\Model\MoviesFactory;


class Delete extends Action
{
    protected $resultPageFactory;

    protected $moviesFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        MoviesFactory $moviesFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->moviesFactory = $moviesFactory;
    }

    public function execute()
    {
        $this->resultPageFactory->create();

        $movieId = $this->getRequest()->getParam('id');

        if ($movieId)
        {
            $movieModel = $this->moviesFactory->create();
            $movieModel->load($movieId);

            // If movie exists or not
            if (!$movieModel->getId())
            {
                $this->messageManager->addError(__('This movie no longer exists'));
            }
            else
            {
                try
                {
                    // Delete this movie
                    $movieModel->delete();
                    $this->messageManager->addSuccess(__('The movie has been deleted'));

                    // Redirect ti grid page
                    $this->_redirect('*/*/');
                    return;
                }
                catch (\Exception $e)
                {
                    $this->messageManager->addError($e->getMessages());
                    $this->_redirect('*/*/edit',['id'=>$movieModel->getId()]);
                }
            }
        }
        // TODO: Implement execute() method.
    }
}