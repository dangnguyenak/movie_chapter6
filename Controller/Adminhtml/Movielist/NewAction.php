<?php

namespace Magenest\Movie\Controller\Adminhtml\Movielist;


use	Magento\Backend\App\Action;
use	Magento\Framework\View\Result\PageFactory;

class NewAction extends Action
{
    /**
     * Create new action
     *
     * @return void
     */
    protected $resultPageFactory;

    public function __construct(
        Action\Context	$context,
        PageFactory	$resultPageFactory
    )	{
        parent::__construct($context);
        $this->resultPageFactory	=	$resultPageFactory;
    }

    public function execute()
    {
        $this->_forward('edit');
    }
}