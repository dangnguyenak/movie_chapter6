<?php
namespace Magenest\Movie\Controller\Adminhtml\Movielist;

use \Magento\Backend\App\Action;
use \Magento\Framework\View\Result\PageFactory;
use \Magenest\Movie\Model\MoviesFactory;



class Save extends Action
{

    protected $_movieFactory;

    protected $resultPageFactory;

    public function __construct(
        Action\Context	$context,
        PageFactory	$resultPageFactory,
        MoviesFactory $movieFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory	=	$resultPageFactory;
        $this->_movieFactory = $movieFactory;
    }

    public function execute()
    {

        $this->resultPageFactory->create();

//         TODO: Implement execute() method.
        $isPost = $this->getRequest()->getPost();

        if ($isPost)
        {
            $movieModel = $this->_movieFactory->create();

            $movieId = $this->getRequest()->getParam('id');
            if ($movieId)
            {
                $movieModel->load($movieId);
            }

            $formData = $this->getRequest()->getParam('movie');
            $movieModel->setData($formData);

            try
            {
                $movieModel->save();

                $this->messageManager->addSuccess(__('This movie has been saved'));

                // If Save and Continue
                if ($this->getRequest()->getParam('back'))
                {
                    $this->_redirect('*/*/edit', ['id' => $movieModel->getId(), '_current' => true]);
                    return;
                }

                // Back to Grid page
                $this->_redirect('*/*/');
                return;
            } catch (\Exception $e)
            {
                $this->messageManager->addError($e->getMessage());
            }

            $this->_getSession()->setFormData($formData);
            $this->_redirect('*/*/edit', ['id' => $movieId]);
        }
    }

}