<?php
namespace Magenest\Movie\Controller\Adminhtml\Actorlist;

use Magento\Backend\App\Action;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{
    protected $resultPageFactory;

    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
        // TODO: Implement execute() method.
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Magenest_Movie::actorlist');
        $resultPage->addBreadcrumb(__('Actors list'), __('Actors list'));
        $resultPage->addBreadcrumb(__('Manage Actors list'), __('Manage Actors list'));

        $resultPage->getConfig()->getTitle()->prepend(__('Actors list'));

        return $resultPage;
    }
}