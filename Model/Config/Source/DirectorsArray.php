<?php
namespace Magenest\Movie\Model\Config\Source;

class DirectorsArray implements \Magento\Framework\Option\ArrayInterface
{
    protected $_directors;

    public function __construct(
        \Magenest\Movie\Model\DirectorsFactory $modelDirector
    )
    {
        $this->_directors = $modelDirector;
    }

    /**
     * Return array of options as value-label pairs
     *
     * @return array Format: array(array('value' => '<value>', 'label' => '<label>'), ...)
     */
    public function toOptionArray()
    {
        $data = array();
        $collections = $this->_directors->create()->getCollection();

        foreach ($collections as $collection)
        {
            $data[] = [
                'value'=>$collection->getId(),
                'label'=> __($collection->getName())
                ];
        }
        return $data;


//
    }
}