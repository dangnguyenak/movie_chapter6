<?php
namespace Magenest\Movie\Model\Config\Source;

class MovieCustomText implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        // TODO: Implement toOptionArray() method.
        $data = array();
        $data [] =
            [
                'value'=>'1',
                'label'=>__('Show')
            ];

          $data[] =  [
                'value'=>'2',
                'label'=>__('Hide')
            ];
        return $data;
    }
}