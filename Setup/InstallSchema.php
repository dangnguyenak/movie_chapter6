<?php

namespace Magenest\Movie\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Create table 'greeting_message'
         */
        $installer = $setup;
        $installer->startSetup();
        $table = $installer->getConnection()->newTable(
                $installer->getTable( 'magenest_movie' )
            )->addColumn(
                'movie_id',
                Table::TYPE_INTEGER,
                null,[
                'identity'=>true,
                'nullable'=>false,
                'primary'=>true
            ],
                'Move ID'
            )->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,[
                'nullable'=>false,
            ],
                'Movie name'
            )->addColumn(
                'description',
                Table::TYPE_TEXT,
                255,[
                'nullable'=>false,
            ],
                'Description'
            )->addColumn(
                'rating',
                Table::TYPE_INTEGER,
                null,[

            ],
                'Rating'
            )->addColumn(
                'director_id',
                Table::TYPE_INTEGER,
                null,[
            ],
                'Director'
            )->setComment(
                    'Movie list here'
                );
            $installer->getConnection()->createTable($table);

            // Create table director
            $table = $installer->getConnection()->newTable(
                $installer->getTable( 'magenest_director' )
            )->addColumn(
                'director_id',
                Table::TYPE_INTEGER,
                null,[
                'identity'=>true,
                'nullable'=>false,
                'primary'=>true
            ],
                'Director ID'
            )->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,[
                'nullable'=>false,
            ],
                'Director name'
            )->setComment(
                'Director'
            );
            $installer->getConnection()->createTable($table);

            // Create table movie_actor
            $table = $installer->getConnection()->newTable(
                $installer->getTable( 'magenest_actor' )
            )->addColumn(
                'actor_id',
                Table::TYPE_INTEGER,
                null,[
                'identity'=>true,
                'nullable'=>false,
                'primary'=>true
            ],
                'Actor ID'
            )->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,[
                'nullable'=>false,
            ],
                'Actor name'
            )->setComment(
                'Actor'
            );
            $installer->getConnection()->createTable($table);

            // Create table movie_actor
            $table = $installer->getConnection()->newTable(
                $installer->getTable( 'magenest_movie_actor' )
            )->addColumn(
                'movie_id',
                Table::TYPE_INTEGER,
                null,[
            ],
                'Movie ID'
            )->addColumn(
                'actor_id',
                Table::TYPE_INTEGER,
                null,[
            ],
                'Actor name'
            )->setComment(
                'Movie Actor'
            );
            $installer->getConnection()->createTable($table);

            $installer->endSetup();

    }
}