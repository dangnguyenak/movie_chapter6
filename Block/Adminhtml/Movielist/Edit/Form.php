<?php
namespace Magenest\Movie\Block\Adminhtml\Movielist\Edit;

/**
 * Adminhtml staff edit form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{

    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;

    protected $_status;

    protected $modelDirectors;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magenest\Movie\Block\Directorlist $directorsArray,
        array $data = []
    ) {
        $this->_systemStore = $systemStore;
        parent::__construct($context, $registry, $formFactory, $data);
        $this->modelDirectors = $directorsArray;
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('movie_form');
        $this->setTitle(__('Movie Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {

        /** @var \Magenest\Movie\Model\Movies $model */
        $model = $this->_coreRegistry->registry('magenest_movie');



        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            [
                'data' =>
                    [
                        'id' => 'edit_form',
                        'action' => $this->getData('action'),
                        'method' => 'post'
                    ]
            ]
        );

        $form->setHtmlIdPrefix('movie_');
        $form->setFieldNameSuffix('movie');

        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );

        if ($model->getId()) {
            $fieldset->addField(
                'movie_id',
                'hidden',
                [
                    'name' => 'movie_id'
                ]);
        }

        $fieldset->addField(
            'name',
            'text',
            [
                'name' => 'name',
                'label' => __('Movie Name'),
                'title' => __('Movie Name'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'description',
                'label' => __('Description'),
                'title' => __('Description'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'rating',
            'select',
            [
                'label' => __('Rating'),
                'title' => __('Rating'),
                'name' => 'rating',
                'required' => true,
                'options' => [1=>'1', 2=>'2', 3=>'3', 4=>'4', 5=>'5']
            ]
        );
        $fieldset->addField(
            'director_id',
            'text',
            [
                'name'=>'director_id',
                'label'=>'Director ID',
                'title'=>'Director ID',
                'required'=>true,
//                'options'=> $this->modelDirectors->getDirectors()
            ]
        );
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}