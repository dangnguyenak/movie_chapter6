<?php
namespace Magenest\Movie\Block\Adminhtml;

class Movielist extends \Magento\Backend\Block\Widget\Grid\Container
{
    public function _construct()
    {
        $this->_blockGroup = 'Magenest_Movie';
        $this->_controller = 'adminhtml_movielist';
        $this->_addButtonLabel = __('New Movie');

        parent::_construct();
    }
}