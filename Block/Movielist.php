<?php
namespace Magenest\Movie\Block;


use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magenest\Movie\Model\Movies;


class Movielist extends Template
{

    public function __construct(Context $context, Movies $model)
    {
        $this->model = $model;
        parent::__construct($context);

    }

    public function getMovieCollection()
    {
        $movieCollection = $this->model->getCollection();
        return $movieCollection;
    }
}